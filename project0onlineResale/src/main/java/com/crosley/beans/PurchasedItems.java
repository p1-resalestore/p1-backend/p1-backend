package com.crosley.beans;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Objects;

public class PurchasedItems {

    private int itemId;
    private int quantity;
    private Date soldDate;
    private double unitPrice;
    private double totalPrice;

    public PurchasedItems() {    }

    public PurchasedItems(int itemId, int quantity, Date soldDate, double unitPrice, double totalPrice) {
        this.itemId = itemId;
        this.quantity = quantity;
        this.soldDate = soldDate;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getSoldDate() {
        return soldDate;
    }

    public void setSoldDate(Date soldDate) {
        this.soldDate = soldDate;
    }

    public double getUnitPrice() { return unitPrice; }

    public void setUnitPrice(double unitPrice) { this.unitPrice = unitPrice; }

    public double getTotalPrice() { return totalPrice; }

    public void setTotalPrice(double totalPrice) { this.totalPrice = totalPrice; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchasedItems that = (PurchasedItems) o;
        return itemId == that.itemId && quantity == that.quantity && Double.compare(that.unitPrice, unitPrice) == 0 && Double.compare(that.totalPrice, totalPrice) == 0 && Objects.equals(soldDate, that.soldDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId, quantity, soldDate, unitPrice, totalPrice);
    }

    @Override
    public String toString() {
        return "PurchasedItems{" +
                "itemId=" + itemId +
                ", quantity=" + quantity +
                ", soldDate='" + soldDate + '\'' +
                ", unitPrice=" + unitPrice +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
