package com.crosley.controllers;


import com.crosley.beans.Items;
import com.crosley.customexceptions.wrongInput;
import com.crosley.services.ItemService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;


public class ItemController {

    private ItemService itemService = new ItemService();

//getting an item bu id
    public int handleGetItemById(Context ctx) throws wrongInput {
        String idString = ctx.pathParam("itemId");
        if(idString.matches("^\\d+$")){
            int idInt = Integer.parseInt(idString);
            Items item = itemService.getItem(idInt);
            if(item == null){
                throw new NotFoundResponse("Item not found");
            }else{
                ctx.json(item);
                return item.getItemId();
            }
        }else{
            throw new NotFoundResponse("Must enter a numeric value");
        }
    }
//getting all items in database
    public void handleGetAllItems(Context ctx){
        ctx.json(itemService.getAll());
    }
//adding an item to database
    public void handlePostNewItem(Context ctx){
       Items item = ctx.bodyAsClass(Items.class);
        itemService.addItem(item);
    }
//updates item in database by id
//to update an item copy an existing item into a post body
//with the URL localhost:7000/items/1/update. Change the 1 to the
//id of the item you would like to update and change the fields
//that need to be updated
    public void handlePostUpdateItem(Context ctx) throws wrongInput {
            Items item = ctx.bodyAsClass(Items.class);
            itemService.updateItem(item);
        }

//removing an item from database by id
    public void handlePostRemoveItemById(Context ctx){
        String idString = ctx.pathParam("itemId");
        if(idString.matches("^\\d+$")){                    //checking for a number with regex
            int idInt = Integer.parseInt(idString);             //id of item to remove
            Items item = itemService.getItem(idInt);
            if(item == null){                                   //checking if item exists
                throw new NotFoundResponse("Item " + idString + " not found");
            }else{
                itemService.removeItem(idInt);
            }
        }else{
            throw new NotFoundResponse("Must enter a numeric value for Id");
        }
    }
}
