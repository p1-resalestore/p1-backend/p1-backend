package com.crosley.controllers;

import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private Logger log = LoggerFactory.getLogger(AuthController.class);

    public void custAuthLogin(Context ctx){
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        String role = ctx.formParam("role");        //customer role is 2
        log.info("attempting to login");
        if(user != null && user.equals("customer")){
            if(pass != null && pass.equals("customerPass") && role.equals("2")){
                ctx.header("Authorization", "customer-auth-token");
                ctx.status(200);
                log.info("login successful");
                return;
            }
        }
        throw new UnauthorizedResponse("Wrong credentials");
    }
    public void custAuthToken(Context ctx) {
        String authHeader = ctx.header("Authorization");
        if(authHeader != null && authHeader.equals("customer-auth-token")){
            log.info("request authorized");
        }else{
            log.warn("improper authorization");
            throw new UnauthorizedResponse("invalid user");
        }
    }

    public void employeeAuthLogin(Context ctx){
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");
        String role = ctx.formParam("role");
        log.info("attempting to login");
        if(user != null && user.equals("employee")){
            if(pass != null && pass.equals("employeePass") && role.equals("1")){
                ctx.header("Authorization", "employee-auth-token");
                ctx.status(200);
                log.info("login successful");
                return;
            }
        }
            throw new UnauthorizedResponse("Wrong credentials");
    }

    public void employeeAuthToken(Context ctx) {
        String authHeader = ctx.header("Authorization");
        if(authHeader != null && authHeader.equals("employee-auth-token")){
            log.info("request authorized");
        }else{
            log.warn("improper authorization");
            throw new UnauthorizedResponse("invalid user");
        }
    }
}
