package com.crosley.controllers;

import com.crosley.beans.Items;
import com.crosley.beans.PurchasedItems;
import com.crosley.data.PurchasedItemDAOImpl;
import com.crosley.services.PurchaseService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class PurchasesController {
    PurchaseService purchaseService = new PurchaseService();
    Logger log = LoggerFactory.getLogger(PurchasesController.class);

//getting all purchased items
    public void handleGetAllPurchases(Context ctx){
        ctx.json(purchaseService.getAll());
    }
//getting a purchased item by id
    public int handleGetPurchase(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$") ){                 //checking for a number with regex
            int id = Integer.parseInt(idString);
            PurchasedItems purchasedItems = purchaseService.getPurchase(id);
            if(purchasedItems == null){
                throw new NotFoundResponse("purchase not found");
            }else {
                ctx.json(purchasedItems);
                return purchasedItems.getItemId();
            }
        }
        throw new NotFoundResponse("Must enter a numeric value");
    }
//making a purchase
    public void handlePostNewPurchase(Context ctx) {
        int[] purchasedInfo = new int[2];                   //array to store item id and quantity to purchase
        String id = ctx.formParam("itemId");
        String quantity = ctx.formParam("quantity");
        purchasedInfo[0] = Integer.parseInt(id);
        purchasedInfo[1] = Integer.parseInt(quantity);
        purchaseService.createPurchase(purchasedInfo);
    }
//removing a purchase
    public void handlePostRemovePurchase(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInt = Integer.parseInt(idString);
            PurchasedItems item = purchaseService.getPurchase(idInt);
            if(item == null){
                throw new NotFoundResponse("Item " + idString + " not found");
            }else{
                purchaseService.removePurchase(idInt);
            }
        }
            throw new NotFoundResponse("Please enter digits for Id");
    }
}
