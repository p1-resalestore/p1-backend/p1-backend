package com.crosley.controllers;

import com.crosley.beans.Items;
import com.crosley.beans.Users;
import com.crosley.customexceptions.wrongInput;
import com.crosley.data.UserDAO;
import com.crosley.data.UserDAOImpl;
import com.crosley.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;

public class UserController {

    UserService userService = new UserService();
//getting all users
    public void handleGetAllUsers(Context ctx) {
        ctx.json(userService.getUsers());
    }

//getting user by id
    public void handleGetUserById(Context ctx) throws wrongInput {
        String userIdString = ctx.pathParam("userId");
        if(userIdString.matches("^\\d+$")){
            int userId = Integer.parseInt(userIdString);
            Users getUser = userService.getUser(userId);
            if(getUser == null){
                throw new NotFoundResponse("User not found");
            }else{
                ctx.json(getUser);
            }
        }
            throw new wrongInput("Must enter a numeric value");
    }

//creating a users
    public void handlePostNewUser(Context ctx){
        Users newUser = ctx.bodyAsClass(Users.class);
        userService.addUser(newUser);
    }

//update a users information
    public void handleUpdateUser(Context ctx){
        Users updateUser = ctx.bodyAsClass(Users.class);
        userService.updateUser(updateUser);
    }

//remove a users by id
    public void handleRemoveUser(Context ctx){
        String idString = ctx.pathParam("userId");
        if(idString.matches("^\\d+$")){                         //checking for numeric input
            int idInt = Integer.parseInt(idString);
            Users users = userService.getUser(idInt)   ;             //getting user to remove
            if(users == null){
                throw new NotFoundResponse("Item " + idString + " not found");
            }else{
                userService.removeUser(idInt);                         //if user is found remove
            }
        }
        throw new NotFoundResponse("Please enter digits for Id");
    }
}
