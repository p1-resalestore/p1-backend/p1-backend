package com.crosley.services;

import com.crosley.beans.Users;
import com.crosley.data.UserDAOImpl;

import java.util.List;

public class UserService {
    UserDAOImpl userDAOImpl = new UserDAOImpl();

    public List<Users> getUsers() {return userDAOImpl.getAllUsers(); }
    public Users getUser(int id) {return userDAOImpl.getUserById(id);}
    public Users addUser(Users users) { return userDAOImpl.addNewUser(users);}
    public Users updateUser(Users users){return userDAOImpl.updateUser(users);}
    public void removeUser(int id ) {userDAOImpl.removeUser(id);}
}
