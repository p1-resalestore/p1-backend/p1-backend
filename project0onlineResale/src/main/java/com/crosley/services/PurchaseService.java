package com.crosley.services;

import com.crosley.beans.Items;
import com.crosley.beans.PurchasedItems;
import com.crosley.controllers.PurchasesController;
import com.crosley.data.PurchasedItemDAOImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class PurchaseService {

    PurchasedItemDAOImpl purchasedItemDAO = new PurchasedItemDAOImpl();
    public List<PurchasedItems> getAll(){ return purchasedItemDAO.getAllPurchases(); }
    public PurchasedItems getPurchase(int id){ return purchasedItemDAO.getPurchasedItemById(id); }
    public void removePurchase(int id){purchasedItemDAO.removePurchasedItem(id);}
    public void createPurchase(int[] purchaseInfo){purchasedItemDAO.addPurchasedItem(purchaseInfo);}
    //public PurchasedItems updatePurchase(PurchasedItems purchasedItems){ return purchasedItemDAO.addPurchasedItem();}
}
