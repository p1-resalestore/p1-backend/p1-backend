package com.crosley.data;

import com.crosley.beans.Items;
import com.crosley.beans.PurchasedItems;

import java.util.List;

public interface PurchasedItemDAO {


    public List<PurchasedItems> getAllPurchases();
    public PurchasedItems getPurchasedItemById(int id);
    public void addPurchasedItem(int[] purchase);
    public void removePurchasedItem(int id);


}
