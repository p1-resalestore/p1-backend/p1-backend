package com.crosley.data;

import com.crosley.beans.Items;
import com.crosley.beans.PurchasedItems;
import com.crosley.util.ConnectionUtilitie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

public class PurchasedItemDAOImpl implements PurchasedItemDAO {
    Logger log = LoggerFactory.getLogger(PurchasedItemDAOImpl.class);

    @Override
    public List<PurchasedItems> getAllPurchases() {
        List<PurchasedItems> purchasedItems = new ArrayList<>();

        log.info("attempting to make connection to retrieve all purchased items");
        try(Connection con = ConnectionUtilitie.getConnection();
            Statement st = con.createStatement()) {
            log.info("connection made, retrieving all purchased items");
            ResultSet rs = st.executeQuery("select * from purchased_item");
            while ((rs.next())){
                int id = rs.getInt("item_id");
                int quantity = rs.getInt("quantity");
                Date date =rs.getDate("date_sold");
                double unitPrice = rs.getDouble("unit_price");
                double totalPrice = rs.getDouble("total_price");
                purchasedItems.add(new PurchasedItems(id, quantity, date, unitPrice, totalPrice) );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return purchasedItems;
    }

    @Override
    public PurchasedItems getPurchasedItemById(int id) {

        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement pdst = con.prepareStatement("select * from purchased_item where item_id = ?")) {

            pdst.setInt(1, id);
            ResultSet rs = pdst.executeQuery();
            if(rs.next()){
                int itemId = rs.getInt("item_id");
                int quantity = rs.getInt("quantity");
                Date date = rs.getDate("date_sold");
                double unitPrice = rs.getDouble("unit_price");
                double totalPrice = rs.getDouble("total_price");
                return new PurchasedItems(itemId,quantity,date,unitPrice,totalPrice);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return null;
    }


    @Override
    //purchases[0] = id purchases[1] = quantity
    public void addPurchasedItem(int[] purchases) {
        //checking if item exists in purchased_item table
        int id = purchases[0];
        int quantity = purchases[1];
        String searchPurchases = "select * from purchased_item where item_id = (select item_id from item where item_id = ?)";
        //if item exists in purchased_item update quantity of item and purchased_item
        String updatePurchaseQuantity = "update purchased_item set quantity = quantity + ? where item_id = ?";
        String updateTotalPrice = "update purchased_item set total_price = quantity * unit_price where item_id = ?";
        String updateItemQuantity = "update item set item_quantity = item_quantity - ?"
                +" where item_id = ?";
        //if item does not exist in purchased_item add item
        String addItemPurchases = "insert into purchased_item values(?, ?, default, ?)";
        String getItem = "select * from item where item_id = ?";
        log.info("Attempting to make a connection to create a purchase");
        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement findItem = con.prepareStatement(searchPurchases);
            PreparedStatement addQuantity = con.prepareStatement(updatePurchaseQuantity);
            PreparedStatement totalPrice = con.prepareStatement(updateTotalPrice);
            PreparedStatement subtractQuantity = con.prepareStatement(updateItemQuantity);
            PreparedStatement addItem = con.prepareStatement(addItemPurchases);
            PreparedStatement getSoldItem = con.prepareStatement(getItem)
            ) {
            findItem.setInt(1, id);
            ResultSet rsFindItem = findItem.executeQuery();
           log.info("checking for item in purchased_item");
            if(rsFindItem.next()){  //if item is found in DB update quantity in item/purchased_item
                                    //and update total sales for purchased item
                con.setAutoCommit(false);
                    addQuantity.setInt(1,quantity);
                    addQuantity.setInt(2, id);
                    totalPrice.setInt(1, id);
                    subtractQuantity.setInt(1, quantity);
                    subtractQuantity.setInt(2, id);
                addQuantity.executeUpdate();
                totalPrice.executeUpdate();
                subtractQuantity.executeUpdate();
                con.commit();
                log.info("purchase complete");
            }else{                  //if item is not in purchases_item add the item
                getSoldItem.setInt(1, id);
                ResultSet purchasedRs = getSoldItem.executeQuery();
                log.info("creating item");
                if (purchasedRs.next()){
                    Items items = new Items(purchasedRs.getInt("item_id"), purchasedRs.getString("item_name"), purchasedRs.getString("item_description"), purchasedRs.getString("item_category"), purchasedRs.getString("item_condition"), purchasedRs.getInt("item_quantity"), purchasedRs.getDouble("item_price"));
                    double price = items.getItemPrice();
                    int itemId = items.getItemId();
                    addItem.setInt(1, itemId );
                    addItem.setInt(2, quantity);
                    addItem.setDouble(3, price);
                    addItem.executeUpdate();
                    subtractQuantity.setInt(1, quantity);
                    subtractQuantity.setInt(2, id);
                    subtractQuantity.executeUpdate();
                    totalPrice.setInt(1, id);
                    totalPrice.executeUpdate();
                    log.info("update executed");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
    }

    @Override
    public void removePurchasedItem(int id) {
        try(Connection con = ConnectionUtilitie.getConnection();
            PreparedStatement pdst = con.prepareStatement("delete from purchased_item where item_id = ?")) {
            pdst.setInt(1, id);
            pdst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
    }
}
