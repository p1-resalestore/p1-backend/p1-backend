package com.crosley.data;

import com.crosley.beans.Items;
import com.crosley.util.ConnectionUtilitie;

import java.net.ConnectException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class ItemDAOImpl implements ItemDAO {

    private Logger log = LoggerFactory.getLogger(ItemDAOImpl.class);
    String connectionMade = "connection made";

    public List<Items> getAllItems() {
        List<Items> items = new ArrayList<>();
        log.info("attemting to make connection to retreive all items");
        try (Connection con = ConnectionUtilitie.getConnection();
             Statement statement = con.createStatement();) {
            log.info("connection successful, getting all items ");
            ResultSet rs = statement.executeQuery("select * from item");
            //adding items to list
            while (rs.next()) {
                int idField = rs.getInt("item_id");
                String itemNameField = rs.getString("item_name");
                String itemDescriptionField = rs.getString("item_description");
                String itemCategoryField = rs.getString("item_category");
                String itemConditionField = rs.getString("item_condition");
                int quantityField = rs.getInt("item_quantity");
                double itemPriceField = rs.getDouble("item_price");
                Items addingItem = new Items(idField, itemNameField, itemDescriptionField, itemCategoryField, itemConditionField, quantityField, itemPriceField);
                items.add(addingItem);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return items;                   //returning list of items
    }

    public Items getItemById(int id) {

        log.info("attemting to make connection to retreive item " + id);
        try (Connection con = ConnectionUtilitie.getConnection();
             PreparedStatement pdst = con.prepareStatement("select * from item where item_id = ?");) {
            log.info(connectionMade);
            pdst.setInt(1, id);
            ResultSet rs = pdst.executeQuery();
            if (rs.next()) {                                    //if a item is found return it
                int itemId = rs.getInt("item_id");
                log.info("getting item " + itemId);
                return new Items(rs.getInt("item_id"), rs.getString("item_name"), rs.getString("item_description"), rs.getString("item_category"), rs.getString("item_condition"), rs.getInt("item_quantity"), rs.getDouble("item_price"));
            }
            else{
                log.info("item " + id + " not found");
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }catch(ClassNotFoundException classNotFoundException){
            classNotFoundException.printStackTrace();
        }
        return null;
    }

    public Items addNewItem(Items item) {
        log.info("attemting to make connection to add item");
        try (Connection con = ConnectionUtilitie.getConnection();
             PreparedStatement pdst = con.prepareStatement("insert into item values (default,?,?,?,?,?,?)");) {
            log.info("connection made, add item");
            pdst.setString(1, item.getItemName());
            pdst.setString(2, item.getItemDescription());
            pdst.setString(3, item.getItemCategory());
            pdst.setString(4, item.getItemCondition());
            pdst.setInt(5, item.getItemQuantity());
            pdst.setDouble(6, item.getItemPrice());
            pdst.executeUpdate();
            log.info("item added");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }

        return item;
    }

    public void removeItem(int id) {
        log.info("attemting to make connection to delete item " + id);
        try (Connection con = ConnectionUtilitie.getConnection();
             PreparedStatement pdst = con.prepareStatement("delete from item where item_id = ?");) {
            log.info(connectionMade);
            pdst.setInt(1, id);
            if (pdst.executeUpdate() == 1) {
                log.info("item " + id + " deleted");
            } else {
                log.info("delete failed");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
    }

    public Items updateItem(Items item) {
        log.info("attemting to make connection to update item " + item.getItemId());
        try (Connection con = ConnectionUtilitie.getConnection();
             PreparedStatement pdst = con.prepareStatement("update item set item_name = ?,item_description = ?, "
                     + " item_category = ?, item_condition = ?, item_quantity = ?, item_price = ?"
                     + " where item_id = " + item.getItemId());) {
            log.info(connectionMade);
            pdst.setString(1, item.getItemName());
            pdst.setString(2, item.getItemDescription());
            pdst.setString(3, item.getItemCategory());
            pdst.setString(4, item.getItemCondition());
            pdst.setInt(5, item.getItemQuantity());
            pdst.setDouble(6, item.getItemPrice());
            pdst.executeUpdate();
            log.info("item updated");
            return item;
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        return null;
    }
}