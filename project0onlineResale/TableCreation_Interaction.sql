create schema online_resale

create table users(
user_id serial primary key,
user_role int not null check (user_role = 1 or user_role = 2),
username varchar(50) not null unique,
userpass varchar(50) not null,
firstname varchar(50) not null,
lastname varchar(50) not null,
address varchar(100),
email varchar(100) not null unique 

);

insert into users values(default, 1, 'CodyC', 'myPass', 'Cody', 'Crosley', '1234 myStreest someCity, Tx', 'CodyC@email.com');
insert into users values(default, 2, 'JohnS', 'myPass', 'John', 'Smith', '673 yourStreest theCity, Tx', 'JohnS@email.com');
insert into users values(default, 2, 'TimJ', 'myPass', 'Tim', 'Jones', '67534 someStreest yourCity, Tx', 'TimJ@email.com');

create table item(
item_id serial primary key ,
item_name varchar(150),
item_description text,
item_category varchar(50),
item_condition varchar,
item_quantity int,
item_price numeric

);

insert into item values(default,'Ring', 'This is a really nice ring!', 'Jewelry', 'new', 2, 100.00);
insert into item values(default,'Nerf Gun', 'This Nerf Gun is a Blast!', 'Toys', 'new', 10, 25.00);
insert into item values(default,'Pink', 'Awesome Pink CD!', 'Media', 'used', 3, 15.00);

create table purchased_item(
item_id int primary key references item (item_id),
quantity int,
date_sold date,
unit_price numeric(8,2) check (unit_price >= 0),
total_price numeric(8,2) check (unit_price >= 0)
);


















