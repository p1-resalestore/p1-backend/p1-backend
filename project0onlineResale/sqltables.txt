item table

item_id			serial PK
item_name		varchar
item_description	text
item_category		varchar
item_condition		varchar
item_quantity		int4
item_price		numeric







users table

user_id			serial PK
user_role		int4
username		varchar
userpass		varchar
firstname		varchar
lastname		varchar
address			varchar
email			varchar







purchased_item table

item_id			int4
quantity		int4
date_sold		date